# football-voronoi

This project aims to produce Voronoi diagrams from football videos and images.
These can be used to analyse the space players are able to create.

This project is currently early in development.

## Example

![voronoi-example](https://gitlab.com/JonWarren95/FootballVoronoi/-/raw/main/data/results/spain-portugal-out.png)

