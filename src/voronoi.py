from src.vision.yolo import run_object_recognition
from src.vision.affiliation import determine_affiliations
from src.display.display import display_overlay
from src.compute_voronoi import compute_voronoi_diagram


def main():
    demo_image = "../data/spain-portugal.jpg"
    run_full_process(demo_image)


def run_full_process(img):
    bboxes = run_object_recognition(img)
    affiliated_detections = determine_affiliations(img, bboxes)
    voronoi_diagram = compute_voronoi_diagram(affiliated_detections)

    display_overlay(img, voronoi_diagram)


if __name__ == "__main__":
    main()
