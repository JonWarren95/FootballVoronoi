from dataclasses import dataclass

from ultralytics import YOLO

PERSON_CLASS = 0


@dataclass
class VisionResult:
    bbox: list
    feet: tuple


def run_object_recognition(img):
    bboxes = get_bboxes(img)

    results = []
    for bbox in bboxes:

        bbox = bbox.tolist()[0]
        feet = bbox[0], bbox[1] + (0.5 * bbox[3])

        results.append(VisionResult(bbox, feet))

    return results


def get_bboxes(img):
    model = YOLO("yolov8n.pt")

    results = model(img, show=False)
    result = results[0]

    names = result.names

    bboxes = []

    for box in result.boxes:
        if box.cls == PERSON_CLASS:
            bboxes.append(box.xywh)
        else:
            print(f"Also detected {names[int(box.cls)]}")

    return bboxes


if __name__ == "__main__":
    run_object_recognition("../../data/spain-portugal.jpg")
