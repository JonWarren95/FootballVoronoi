from dataclasses import dataclass


@dataclass
class AffiliatedDetection:
    bbox: list
    feet: tuple
    affiliation: int


def determine_affiliations(img, bboxes):
    # categorise each detection as playing for Team1, Team2 or Official based on the person's colours
    affiliated_detections: list[AffiliatedDetection] = []

    for detection in bboxes:
        # TODO
        # all affiliations are set to 0 for now
        affiliated_detection = AffiliatedDetection(detection.bbox, detection.feet, 0)
        affiliated_detections.append(affiliated_detection)

    return affiliated_detections
