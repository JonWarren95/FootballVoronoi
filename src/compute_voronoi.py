from dataclasses import dataclass

import numpy as np
import freud

from src.vision.affiliation import AffiliatedDetection


@dataclass
class VoronoiCell:
    vertices: list
    site: tuple


def compute_voronoi_diagram(affiliated_detections: list[AffiliatedDetection]):

    sites = np.array([detection.feet for detection in affiliated_detections])

    z_dim = np.zeros(sites.shape[0]).reshape(-1,1)
    sites_3d = np.hstack([sites, z_dim])

    # compute the bounds of the voronoi sites in order to construct a sufficiently large box
    min_x, max_x = np.min(sites[:,0]), np.max(sites[:,0])
    min_y, max_y = np.min(sites[:,1]), np.max(sites[:,1])

    x_diff = max_x - min_x
    y_diff = max_y - min_y

    largest_dim = max(x_diff, y_diff)
    box_dims = 2 * largest_dim

    box = freud.box.Box.square(box_dims)
    voronoi = freud.locality.Voronoi()

    voronoi = voronoi.compute((box, sites_3d))

    diagram: list[VoronoiCell] = []

    for site,poly in zip(sites, voronoi.polytopes):
        cell = VoronoiCell(poly[:,:2], site)
        diagram.append(cell)

    return diagram
