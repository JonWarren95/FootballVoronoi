import cv2


def display_overlay(img, voronoi_diagram):
    img = cv2.imread(img)

    for cell in voronoi_diagram:
        integer_site = [int(i) for i in cell.site]
        cv2.circle(img, integer_site, 20, 0)

        poly = cell.vertices.reshape(-1, 1, 2).astype(int)
        cv2.polylines(img, [poly], True, (0, 0, 255))

    cv2.imshow("Voronoi", img)
    cv2.waitKey(0)


def main():
    img = "../../data/spain-portugal.jpg"
    vd = []
    display_overlay(img, vd)


if __name__ == "__main__":
    main()
